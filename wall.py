import pyglet, math
from pyglet.window import key
from pyglet.gl import *
import resources, physicalobject
import random
import numpy as np

img=pyglet.image.load("./resources/wall.jpg")

class Wall:
	global img

	def __init__(self, x1, y1, x2, y2, batch=None):
		self.x1 = x1
		self.y1 = y1
		self.x2 = x2
		self.y2 = y2

		self.image = pyglet.sprite.Sprite(img=img, batch=batch)
		self.image.x = x1
		self.image.y = y1

	
	def intersect(self, angle, px, py):
		if math.sin(angle) == 0 or math.cos(angle) == 0:
			return 0

		dx = -math.cos(angle)
		dy = math.sin(angle)

		atoo = ( (px - self.x1), (py - self.y1) )	
		atob = ( (self.x2 - self.x1), (self.y2 - self.y1) )
		ortho = (dx, dy)

		denom = np.dot( atob, ortho )
		if denom == 0:
			return 0
		breakpoint()
		t = np.cross(atob, atoo) / denom 
		t2 = np.dot(atoo, ortho) / denom

		return t if t2 >= 0 and t2 <= 1 and t >= 0 else 0



		txmin = (self.x1 - px) / dx
		txmax = (self.x2 - px) / dx

		tymin = (self.y1 - py) / dy
		tymax = (self.y2 - py) / dy

		if txmin > txmax:
			tmp = txmin
			txmin = txmax
			txmax = tmp
			
		if tymin > tymax:
			tmp = tymin
			tymin = tymax
			tymax = tmp

		if (txmin > tymax) or (tymin > txmax):
			return 0

		tmin = txmin
		tmax = txmax

		if tymin > txmin:
			tmin = tymin

		if tymax > txmax:
			tmax = tymax
		
		if tmax < tmin:
			t = tmax
		else:
			t = tmin

		return t
