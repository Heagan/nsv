from pyglet.window import key
import resources
import physicalobject
import numpy as np
import math

from util import eKey
from util import distance
from sensor import Sensor


class Point(physicalobject.PhysicalObject):
	def __init__(self, *args, **kwargs):
		super(Point, self).__init__(img=resources.point_image, *args, **kwargs)

		self.key_handler = key.KeyStateHandler()
		self.event_handlers = [self, self.key_handler]
		self.key = 0

		self.speed = 5
		self.max_speed = 5

		self.brain = None

		self.sensor = []
		self.totalSensors = 5
		self.sensorAngle = (math.pi * 2) / self.totalSensors 
		
		angle = -1
		for _ in range(self.totalSensors):
			self.sensor.append(Sensor(angle))
			angle += 0.5
			
		# angle = 0
		# while angle < math.pi * 2 / 70:
		# 	self.sensor.append(Sensor(angle))
		# 	angle += self.sensorAngle


	def collides_with(self, other_object):
		collision_distance = self.image.width/2 + other_object.image.width/2
		actual_distance = distance(self.position, other_object.position)

		return (actual_distance <= collision_distance)

	def collides_with_any(self, objs):
		col = False
		for obj in objs:
			if self.collides_with(obj):
				col = True
		return col

	def intersect(self, walls):
		t_max = 1000
		col = False
		for s in self.sensor:
			s.t = t_max
			for w in walls:
				s.collides(self.x, self.y, w)
				if s.t < 15:
					col = True
			if s.t > t_max:
				s.t = 0
		return col

	# Process input
	def process(self, dt, n, vel=False):
		if vel:
			if n == eKey["UP"]:
				self.velocity_y += self.speed
			if n == eKey["DOWN"]:
				self.velocity_y -= self.speed
			if n == eKey["LEFT"]:
				self.velocity_x -= self.speed
			if n == eKey["RIGHT"]:
				self.velocity_x += self.speed
			return
		if n == eKey["UP"]:
			self.rotation = 90
			self.y += self.speed
		if n == eKey["DOWN"]:
			self.rotation = 270
			self.y -= self.speed
		if n == eKey["LEFT"]:
			self.rotation = 0
			self.x -= self.speed
		if n == eKey["RIGHT"]:
			self.rotation = 180
			self.x += self.speed

	def update(self, dt):
		super(Point, self).update(dt)

		if self.velocity_x > 0 and self.velocity_y > 0:
			velocity_d = math.atan(self.velocity_y/self.velocity_x)

		# Rotate Sensors
		if self.rot:
			# self.rot = False
			for s in self.sensor:
				s.dx = -math.cos(-math.radians(self.rotation) + s.angle)
				s.dy = -math.sin(-math.radians(self.rotation) + s.angle)
		self.key = 0
		if self.key_handler[key.UP]:
			self.rotation = 90
			self.key = eKey["UP"]
		if self.key_handler[key.DOWN]:
			self.rotation = 270
			self.key = eKey["DOWN"]
		if self.key_handler[key.LEFT]:
			self.rotation = 0
			self.key = eKey["LEFT"]
		if self.key_handler[key.RIGHT]:
			self.rotation = 180
			self.key = eKey["RIGHT"]
		self.process(dt, self.key)
