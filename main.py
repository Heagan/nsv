from wall import Wall
from util import eKey
from perlin import make_map
from point import Point
from vehicle import Vehicle
from brain import Nero
from models.knearest import Brain

import pyglet
import numpy as np
from math import pi
from sys import argv
from time import time

WINDOW_WIDTH = 600
WINDOW_HEIGHT = 600

GAME_WINDOW = pyglet.window.Window(WINDOW_WIDTH, WINDOW_HEIGHT)
MAIN_BATCH = pyglet.graphics.Batch()
KEYBOARD = pyglet.window.key.KeyStateHandler()
GAME_WINDOW.push_handlers(KEYBOARD)

EVENT_STACK_SIZE	= 0
WALLS				= []
CARS 				= []

FD 					= None
CSV 				= []

POV = (90 * 180) / pi
FOV = (70 * 180) / pi

BRAINS = []
WRITE = False
APPEND = False
DATA_FILE = 'data/data.point.minimal.7.CSV'

TIME = time()

nero_brain = Brain(5, 2)#Nero(8, 1, 5)

def load_map():
	global WALLS, MAIN_BATCH
	
	x = 0
	y = 0
	x1 = 0
	y1 = 0
	x2 = 0
	y2 = 0
	filename = open("resources/map2.map", "r")
	for line in filename.read():
		if line == '\n':
			x = -50
			y += 50
		if line == "1":
			WALLS.append(Wall(x - 25, y - 25, x + 25, y + 25, batch=MAIN_BATCH))
		x += 50
	filename.close()

def create_cus_map():
	WALLS.append(Wall(50, 450, 50, 200))
	WALLS.append(Wall(50, 200, 150, 50))
	WALLS.append(Wall(150, 50, 600, 50))
	WALLS.append(Wall(600, 50, 750, 200))
	WALLS.append(Wall(750, 200, 750, 450))
	WALLS.append(Wall(600, 600, 750, 450))
	WALLS.append(Wall(50, 450, 200, 600))

	WALLS.append(Wall(200, 600, 600, 600))

	WALLS.append(Wall(200, 350, 200, 300))
	WALLS.append(Wall(200, 350, 300, 450))
	WALLS.append(Wall(200, 300, 300, 150))

	WALLS.append(Wall(500, 450, 300, 450))
	WALLS.append(Wall(500, 150, 300, 150))

	WALLS.append(Wall(500, 450, 650, 350))
	WALLS.append(Wall(500, 150, 650, 300))

	WALLS.append(Wall(650, 350, 650, 300))

def create_perlin_map(points):
	global WALLS

	if len(points) == 0:
		return
	start = points[0]
	last = start

	for p in points:
		WALLS.append(Wall(last[0], last[1], p[0], p[1]))
		last = p
	WALLS.append(Wall(last[0], last[1], start[0], start[1]))

def make_perlin_map():
	create_perlin_map(make_map(200))
	create_perlin_map(make_map(300))
	
def init():
	global EVENT_STACK_SIZE, FD, WRITE, CARS, nero_brain

	# Neural Network Part
	if WRITE or APPEND:
		if APPEND:
			FD = open(DATA_FILE, "a")
		elif WRITE:
			FD = open(DATA_FILE, "w+")
	else:
		import pandas as pd
		import numpy as np
		dataset = pd.read_csv(DATA_FILE, ';', header=None)
		inputs = len(dataset.columns) - 1
		X = dataset.iloc[:, :inputs:1].values
		y = dataset.iloc[:, -1].values
		for i in range(len(X)):
			# breakpoint()
			nero_brain.add(X[i], y[i])
	# load_map()
	# create_cus_map()
	make_perlin_map()

	# Clear the event stack of any remaining handlers from other levels
	while EVENT_STACK_SIZE > 0:
		GAME_WINDOW.pop_handlers()
		EVENT_STACK_SIZE -= 1

	x=200
	y=100
	# Initialize the player sprite
	if len(BRAINS) == 0:
		car = Vehicle(x=x, y=y, batch=MAIN_BATCH)
		CARS.append(car)
	for brain in BRAINS:
		car = Vehicle(x=x, y=y, batch=MAIN_BATCH)
		car.brain = brain
		CARS.append(car)

	# Add any specified event handlers to the event handler stack
	for handler in car.event_handlers:
		GAME_WINDOW.push_handlers(handler)
		EVENT_STACK_SIZE += 1

def draw_sensors():
	for car in CARS:
		for s in car.sensor:
			x1 = car.x
			y1 = car.y
			x2 = car.x + s.dx * s.t
			y2 = car.y + s.dy * s.t
			pyglet.graphics.draw(4, pyglet.gl.GL_LINES, ("v2f", (x1, y1, x2, y2, x1, y1, x2, y2)))

def draw_walls():
	for w in WALLS:
		x1 = w.x1
		y1 = w.y1
		x2 = w.x2
		y2 = w.y2
		pyglet.graphics.draw(4, pyglet.gl.GL_LINES, ("v2f", (x1, y1, x2, y2, x1, y1, x2, y2)))

@GAME_WINDOW.event
def on_draw():
	GAME_WINDOW.clear()
	pyglet.gl.glLineWidth(2)

	draw_sensors()
	draw_walls()
	
	MAIN_BATCH.draw()

def t_convert(t):
	return 0 if t < 10 else 1 if t < 20 else 2 if t < 30 else 3 if t < 40 else 4 if t < 50 else 5 if t < 60 else 6 if t < 70 else 7 if t < 80 else 8 if t < 90 else 9

def update(dt):
	global FD, CSV, WALLS, WRITE, APPEND, CARS, TIME, nero_brain

	for car in CARS:
		inter = car.intersect(WALLS)
		car.update(dt)

		# Write data!
		if WRITE or APPEND and not inter:
			if car.key > 0:
				CSV = ""
				for s in car.sensor:
					CSV += str(t_convert(s.t)) + ";"
				CSV += str(car.key) + "\n"
				FD.write(CSV)
				print(f"\rWRITING: [{CSV}]", end='' )

		if BRAINS:
			# Predict using data!
			if not WRITE and not APPEND:
				# if BRAIN:
				CSV = []
				for s in car.sensor:
					CSV.append( t_convert(s.t) )
				CSV = np.array(CSV).reshape(1, -1)
				pv = round( nero_brain.predict(np.array(CSV[0])) )
				# pv = nero_brain.best(np.array(CSV[0]))[0]
				
				print(pv, len(nero_brain.points))
				t = time() - TIME
				if car.key == 0 and t > 0.1:
					car.process(dt, pv)
					TIME = time()

def process_args():
	global BRAINS, WRITE, APPEND

	if 'w' in argv:
		WRITE = True
	if 'a' in argv:
		APPEND = True
	if 'svm' in argv:
		BRAINS.append('svm')
	if 'rfr' in argv:
		BRAINS.append('rfr')
	if 'mlr' in argv:
		BRAINS.append('mlr')
	if 'ann' in argv:
		BRAINS.append('ann')
	if 'tnn' in argv:
		BRAINS.append('tnn')
	if 'ten' in argv:
		BRAINS.append('ten')
	if 'brn' in argv:
		BRAINS.append('brn')
				
if __name__ == "__main__":

	process_args()
	# Start it up!
	init()

	# Update the game 120 times per second
	pyglet.clock.schedule_interval(update, 1 / 120.0)

	pyglet.app.run()

































