
out = open('clear_data.csv', 'w+')

f = open('data.csv', 'r')
s = 's'
for s in f.readlines():
	slt = s.split(';')
	slt[0] = float(slt[0]) % 8 * (-1 if float(slt[0]) < 0 else 1)
	slt[1] = float(slt[1]) % 8 * (-1 if float(slt[1]) < 0 else 1)
	# print(slt[1])
	# exit(0)

	r = slt[2::][::-1][1::][::-1]
	n = 0
	for w in r:
		if float(w) < 50:
			slt[2 + n] = '0'
		elif float(w) < 100:
			slt[2 + n] = '1'
		elif float(w) < 200:
			slt[2 + n] = '2'
		elif float(w) < 350:
			slt[2 + n] = '3'
		else:
			slt[2 + n] = '4'
		n += 1

	for i in range(len(slt)):
		out.write(str(slt[i]))
		if i < len(slt) - 1:
			out.write(';')
		# else:
		# 	out.write('\n')
f.close()
out.close()
