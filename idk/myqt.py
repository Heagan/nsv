import numpy as np


MAP = np.zeros([4, 4])
f = open('qmap', 'r')
y = 0
for line in f.readlines():
	x = 0
	for letter in line:
		idx = 1 if letter == 'S' else 0 if letter == 'F' else -1 if letter == 'H' else 2 if letter == 'G' else 3
		if idx == 3:
			break
		MAP[x, y] = 1 if letter == 'S' else 0 if letter == 'F' else -1 if letter == 'H' else 2
		x += 1
	y += 1

'''
G - 100
F - 0
H - -1
'''

while True:
	for y in MAP:
		for x in MAP:
			print(x, end=' ')
	exit(0)
































