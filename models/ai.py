import numpy as np
from NeuralNetwork import *

# simple XOR dataset
data = [[1, 0], [0, 1], [1, 1], [0, 0]]
labels = [[1], [1], [0], [0]]

datasize = len(data)
# initialize the Neural Network
brain = NeuralNetwork(2, 4, 1, epochs=20000, learning_rate=0.1)
brain.setLearningRate(.1)

# training loop
brain.fit(data, labels)
		
print("")

# guessing loop
for i in range(datasize):
	info = data[i]
	goal = labels[i]
	guess = brain.process(info)
	error = brain.mse(info, goal)
	print("answer: %d, guess: %f, error: %s" % (goal[0], 
												guess, 
												error))
a = "-"
while a != "":
	a = input("-> ")
	if len(a) > 2:
		print( brain.process( [int(a[0]), int(a[2])]) )

