# Convolutional Neural Network

# Importing the Keras libraries and packages
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense

from sklearn.preprocessing import StandardScaler
import pandas as pd

# Initialising the CNN
classifier = Sequential()

# Step 1 - Convolution
classifier.add(Convolution2D(4, 3, 3, input_shape = (8, 8, 3), activation = 'relu'))

# Step 2 - Pooling
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Adding a second convolutional layer
classifier.add(Convolution2D(4, 3, 3, activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))

# Step 3 - Flattening
classifier.add(Flatten())

# Step 4 - Full connection
classifier.add(Dense(units = 16, activation = 'relu'))
classifier.add(Dense(units = 1, activation = 'sigmoid'))

# Compiling the CNN
classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# Part 2 - Fitting the CNN to the data
sc = StandardScaler()

dataset = pd.read_csv('data.csv', ';')
X = dataset.iloc[:, 1:(len(dataset.columns) - 1):1].values
y = dataset.iloc[:, -1].values

inputs = (len(dataset.columns) - 1)

X = sc.fit_transform(X)

classifier.fit(X, y, validation_split = 0.2, shuffle = True)