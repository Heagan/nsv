# Multiple Linear Regression

regressor = None

dataset = []
X = []
y = []
sc_X = None

def setup_ml(data_file):
	import numpy as np
	import matplotlib.pyplot as plt
	import pandas as pd

	from sklearn.model_selection import train_test_split
	from sklearn.linear_model import LinearRegression
	from sklearn.preprocessing import StandardScaler

	print("Setting up ML")

	global regressor, X, y, dataset, sc_X

	dataset = pd.read_csv(data_file, ';')
	X = dataset.iloc[:,  	:(len(dataset.columns) - 1):1].values
	y = dataset.iloc[:, -1].values

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.05, random_state = 0)

	sc_X = StandardScaler()
	X_train = sc_X.fit_transform(X_train)
	X_test = sc_X.transform(X_test)

	regressor = LinearRegression()
	regressor.fit(X_train, y_train)

	print("Done! Ready to predict!")

def predict(X):
	global regressor, sc_X

	X = sc_X.transform(X)
	return regressor.predict(X)
