# Artificial Neural Network
classifier = None
sc = None

def setup_ml(data_file):
	import numpy as np
	import matplotlib.pyplot as plt
	import pandas as pd
	from keras.utils import to_categorical
	from keras.models import Sequential
	from keras.layers import Dense
	from sklearn.model_selection import train_test_split
	from sklearn.preprocessing import StandardScaler
	print("Setting up ML")

	global classifier, sc

	sc = StandardScaler()

	dataset = pd.read_csv(data_file, ';')
	inputs = len(dataset.columns) - 1 
	X = dataset.iloc[:, :inputs:1].values
	y = dataset.iloc[:, -1].values
	
	# Splitting the dataset into the Training set and Test set

	# Feature Scaling          
	X = sc.fit_transform(X)
	#X_test = sc.transform(X_test)
	#sc_y = StandardScaler()
	#y_train = sc_y.fit_transform(y_train.reshape(-1, 1))

	classifier = Sequential()

	# Add Input Layer AND First Hidden Layer
	classifier.add(Dense(
			# Number Of Hidden Layer Nodes
			units = inputs,
			# Init weights close to 0
			kernel_initializer = 'uniform',
			# Activation Func In Hidden Layer, relu -> rectifier
			activation = 'relu',
			# Number Of Input Nodes
			input_dim = inputs
			))

	# Dont include input node no. as it already knows how many there will be

	# Add Output Layer
	# for i in range(10):
	# 	classifier.add(Dense(
	# 		units = 100,
	# 		kernel_initializer = 'uniform',
	# 		activation = 'tanh',
	# 	))
	classifier.add(Dense(
			units = 1,
			kernel_initializer = 'uniform',
			activation = 'relu'
		))

	# Suftmax - is sigmoid but for more catergories
	classifier.compile(
			# Optimiser - Algo to use to find the best weight values
			optimizer = 'adam',
			# Loss Function -> eg. used to optimise linear regression model
			# categorical_crossentropy for multiple outputs
			# binary_crossentropy
			# sparse_categorical_crossentropy
			loss = 'binary_crossentropy',
			# Metricts - Criterian used to evaluate your model
			# Uses this to try increase performance
			metrics = ['accuracy']
			)
	
	classifier.fit(X, y, batch_size = inputs, epochs = 20, validation_split = 0.1, shuffle = True)
	# evaluate the model
	scores = classifier.evaluate(X, y, verbose=0)
	print("%s: %.2f%%" % (classifier.metrics_names[1], scores[1]*100))
	# if scores[1]*100 < 80:
	# 	setup_ml(data_file)

	# exit()
	print("Done! Ready to predict!")



def predict(X):
	global classifier, sc

	X = sc.transform(X)

	return classifier.predict(X)