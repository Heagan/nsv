
brain = None
sc = None

def setup_ml(data_file, load=False):
	import os, random

	import pandas as pd
	import numpy as np

	from keras.models import Sequential
	from keras.layers import Dense
	from keras.wrappers.scikit_learn import KerasClassifier
	from keras.utils import np_utils
	from sklearn.model_selection import cross_val_score
	from sklearn.model_selection import KFold
	from sklearn.preprocessing import LabelEncoder
	from sklearn.preprocessing import StandardScaler
	from sklearn.model_selection import train_test_split
	from sklearn.pipeline import Pipeline

	global brain, sc

	print("Setting up Tenflow Neural Network")

	np.set_printoptions(threshold=np.inf)
	dataset = pd.read_csv(data_file, ';', header=None)
	inputs = len(dataset.columns) - 1
	X = dataset.iloc[:, :inputs:1].values
	y = dataset.iloc[:, -1].values

	X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.15)

	sc = StandardScaler()

	X_train = sc.fit_transform(X_train)
	X_test = sc.transform(X_test)
	
	# encode class values as integers
	# encoder = LabelEncoder()
	# encoder.fit(Y)
	# encoded_Y = encoder.transform(Y)
	# convert integers to dummy variables (i.e. one hot encoded)
	dummy_y = np_utils.to_categorical(y)
	# print(y)
	# print('')
	# print(dummy_y)

	# define baseline brain
	def baseline_brain():
		# create brain
		brain = Sequential()
		brain.add(Dense(inputs, input_dim=inputs, activation='relu'))
		for n in range(100):
			brain.add(Dense(int(inputs / 4), activation='relu'))
		brain.add(Dense(inputs, activation='relu'))
		# brain.add(Dense(int(inputs / 2), input_dim=inputs, activation='relu'))
		brain.add(Dense(4, activation='softmax'))
		# Compile brain
		brain.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
		return brain
	print("Parsed Data")
	brain = KerasClassifier(build_fn=baseline_brain, epochs=3, batch_size=1)
	# kfold = KFold(n_splits=10, shuffle=True)
	# , cv=kfold
	results = cross_val_score(brain, X, dummy_y)
	brain.fit(X, dummy_y)
	print("Baseline: %.2f%% (%.2f%%)" % (results.mean()*100, results.std()*100))

def predict(X):
	global brain, sc

	X = sc.transform(X)
	return brain.predict(X)

# setup_ml()

# while True:
# 	s = input("-> ")
# 	if len(s) == 0:
# 		break
# 	print(predict(s))

