from math import sqrt
from random import random

class Points():
    """Helper Class for the Brain to help it store information easier and in a more human friendly way"""
    def __init__(self, inputs, outputs):
        """
        A point is defined as having Inputs that lead to an Output
        Each input represent a point in a dimension.
        These inputs create a vector of n dimensions that point to a single point
        The Output would be at that vectors point
        """
        self.inputs = inputs
        self.outputs = outputs
        self.distance = 0


class Brain():
    """
    Main Class used to store information used to make predictions.
    A prediction is made by:
     - Converting the supplyed inputs to a new point
     - Getting the distance of each saved point to the new point
     - Returning the closest K points
    """


    def __init__(self, inputs: int, K: int):
        """
        Inputs - How many 'dimentions' will there be
        K - How many points to return when making a prediction (lower <better>)
        Dimension - Stores all the points in an array
        """
        self.K = K
        self.inputs = inputs
        self.points = []

    def add(self, inputs, outputs):
        """
        Takes the inputs and outputs then creates a Point out of it.
        The inputs have to be the same length as specified when creating the Class
        *Note* no outputs length is nessesary as it can be whatever length you want when creating points 
        """
        if len(inputs) != self.inputs:
            raise Exception(f"Inputs len invalid expected {self.inputs} found {len(inputs)}")

        self.points.append(Points(inputs, outputs))

    def knearest(self, inputs):
        """
        Returns closest K points from the inputs given.
        Returns distances {<Distance Amount>, <Point information>}
        """
        if len(inputs) != self.inputs:
            raise Exception(f"Inputs len invalid expected {self.inputs} found {len(inputs)}")

        distances = {}
        for point in self.points:
            sqr = 0
            for i in range(self.inputs):
                sqr += (point.inputs[i] - inputs[i]) * (point.inputs[i] - inputs[i])
            distance = sqrt(sqr) + (random() / 100)
            distances[distance] = point

        # sort distance, smallest to biggest
        distances = {k: v for k, v in sorted(list(distances.items()), key=lambda item: item[0])}

        # get K, if K is more than amount of points just pick points
        K = min(len(distances.keys()), self.K)

        # get a list of the first K points
        nearest = list(distances.keys())[:K]

        prediction = {}
        for i in nearest:
            # Get the saved Point data <we want the output>
            point = distances[i]
            # This counts how many of the same output we find
            if point.outputs in prediction:
                prediction[point.outputs] += 1
            else:
                prediction[point.outputs] = 1

        return prediction

    def best(self, inputs):
        """
        Returns: (<best predicted value>, <confidence %>, <all predictions calculated>)
        """
        prediction = self.knearest(inputs)

        # Sort predictions according to their value
        sorted_prediction = {k: v for k, v in sorted(list(prediction.items()), key=lambda item: item[1])}
        # Used to calculate Confidence percentage
        percent = sum(sorted_prediction.values())

        sorted_prediction = list(sorted_prediction.keys())
        sorted_prediction.reverse()
        best_prediction = sorted_prediction[0]
        # breakpoint()
        return (best_prediction, prediction[best_prediction] / percent, sorted_prediction)

    def predict(self, inputs):
        """
        Returns closest K points from the inputs given.
        Returns distances {<Distance Amount>, <Point information>}
        """
        if len(inputs) != self.inputs:
            raise Exception(f"Inputs len invalid expected {self.inputs} found {len(inputs)}")
        
        # calculate the distance to each Point
        points_distances = {}
        for point in self.points:
            sqr = 0
            for i in range(self.inputs):
                sqr += (point.inputs[i] - inputs[i]) * (point.inputs[i] - inputs[i])
            # the random here is to make sure that 2 equal distances are actually slightly different, by atleast 0.000% different <hopfully this doest effect results too much
            distance = sqrt(sqr) + random()
            points_distances[point] = distance

        # sort distance, smallest to biggest
        sorted_points_distances = {k: v for k, v in sorted(list(points_distances.items()), key=lambda item: item[1])}

        # get K, if K is more than amount of points just pick points
        K = min(len(sorted_points_distances.values()), self.K)
        # get a list of the first K points
        nearest_distances = list(sorted_points_distances.values())[:K]
        # breakpoint()
        # Get the saved Point data <we want the output>
        nearest_points = []
        for point, distance in sorted_points_distances.items():
            if distance in nearest_distances:
                point.distance = distance
                nearest_points.append(point)
        # breakpoint()
        largest_distance = max(nearest_distances)
        data = []
        weights = []
        for point in nearest_points:
            # normalize the distances
            # but make the larger the distance the smaller the weight
            # makes the distance a value between 0 - 1
            weight = (1 - (point.distance / largest_distance))
            weights.append(weight)
            data.append(point.outputs * weight)

        # breakpoint()
        if sum(weights) == 0:
            return 0
        return sum(data) / sum(weights)


    def save_brain(self, filename):
        from pickle import dump
        file_obj = open(filename, 'wb')
        dump(self, file_obj)
        file_obj.close()

    def load_brain(self, filename):
        from pickle import load
        file_obj = open(filename, 'rb')
        self.__dict__.update(load(file_obj).__dict__)
        file_obj.close()
