# Random Forest Regression
from time import time

regressor = None

dataset = []
X = []
y = []
sc_X = None


def setup_ml(data_file):
  import numpy as np
  import matplotlib.pyplot as plt
  import pandas as pd

  from sklearn.model_selection import train_test_split
  from sklearn.ensemble import RandomForestRegressor
  from sklearn.preprocessing import StandardScaler

  print("Setting up ML")

  global regressor, X, y, dataset, sc_X

  dataset = pd.read_csv(data_file, ';')
  inputs = len(dataset.columns) - 1
  X = dataset.iloc[:, :inputs:1].values
  y = dataset.iloc[:, -1].values

  # X_train, X_test, y_train, y_test = train_test_split(X, y)

  # sc_X = StandardScaler()
  # X_train = sc_X.fit_transform(X_train)

  regressor = RandomForestRegressor(n_estimators=500)
  # regressor.fit(X_train, y_train)
  regressor.fit(X, y)

  print("Done! Ready to predict!")


ans = None
t = None
def predict(X):
  global regressor, sc_X, t, ans
  # X = sc_X.transform(X)
  delay = 1
  if t == None or time() - t > delay:
      t = time()
      ans = regressor.predict(X)

  return ans
