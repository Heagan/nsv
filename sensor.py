from math import atan2

class Sensor:
	def __init__(self, angle):
		self.angle = angle
		self.dx = 0
		self.dy = 0
		self.t = 1000

	def collides(self, px, py, w):

		angle = atan2(self.dx, self.dy)

		d = w.intersect(angle, px, py)
		if d <= 0:
			return self.t
		if d < self.t:
			self.t = d
		return self.t
