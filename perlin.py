from math import cos, sin, pi
from random import seed, random
from time import time

from noise import pnoise2, snoise2, pnoise3
import numpy as np


seed(time())

def make_map(size = 200, n_points = 16):
	points = []

	a = 0
	while (a < pi * 2):
		a += (pi * 2) / n_points

		xoff = cos(a) 
		yoff = sin(a)
		r = (pnoise3(xoff, yoff, random()) * 100) + size
		x = ( r * cos(a) ) + 300
		y = ( r * sin(a) ) + 300

		points.append( (x, y) )

	return points
























