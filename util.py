from math import sqrt
import numpy as np
import pyglet

eKey = {
	"UP": 1,
	"DOWN": 2,
	"LEFT": 3,
	"RIGHT": 4,
}


def unit_vector(vector):
	return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
	v1_u = unit_vector(v1)
	v2_u = unit_vector(v2)
	return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

def distance(point_1=(0, 0), point_2=(0, 0)):
	"""Returns the distance between two points"""
	return sqrt(
		(point_1[0] - point_2[0]) ** 2 +
		(point_1[1] - point_2[1]) ** 2)

def center_image(image):
	"""Sets an image's anchor point to its center"""
	image.anchor_x = image.width / 2
	image.anchor_y = image.height / 2
