# NeuroEvo Steering Vehicles

Using machine learning and neuro evolution to make self driving vehicles

Written in python, uses tensor flow as a base

Also runs with multiple different learning algorithms:
- Random Forest Regression
- Support Vector Machine
- Multiple Linear Regression

- Artificial Neural Network
- Convolutional Neural Network

# TODO
Add my own neural network library

# How it looks
![Example](https://gitlab.com/Heagan/nsv/raw/master/car.png "Example")

# To Run
pip install -r requirements.txt
./python maze.py


