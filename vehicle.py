import pyglet, math
from pyglet.window import key
import resources, physicalobject, util
import numpy as np

def distance(point_1=(0, 0), point_2=(0, 0)):
	return math.sqrt(
		(point_1[0] - point_2[0]) ** 2 +
		(point_1[1] - point_2[1]) ** 2)

class Sensor:
	def __init__(self, angle):
		self.angle = angle
		self.dx = 0
		self.dy = 0
		self.t = 1000

	def collides(self, px, py, w):

		angle = math.atan2(self.dx, self.dy)

		d = w.intersect(angle, px, py)
		if d <= 0:
			return self.t
		if d < self.t:
			self.t = d
		return self.t


class Vehicle(physicalobject.PhysicalObject):
	def __init__(self, *args, **kwargs):
		super(Vehicle, self).__init__(img=resources.car_image, *args, **kwargs)

		self.brain = None

		self.rotation = 0#180
		self.rotate_speed = 100.0
		self.speed = 150
		self.max_speed = 150

		self.friction = 0.1

		self.key_handler = key.KeyStateHandler()
		self.event_handlers = [self, self.key_handler]

		self.totalSensors = 5
		self.sensorAngle = (math.pi * 2) / self.totalSensors
		self.sensor = []

		self.key = 0

		angle = -1
		for _ in range(self.totalSensors):
			self.sensor.append(Sensor(angle))
			angle += 0.5
			
		# angle = 0
		# while angle < math.pi * 2:
		# 	self.sensor.append(Sensor(angle))
		# 	angle += self.sensorAngle

	def collides_with(self, other_object):
		collision_distance = self.image.width/2 + other_object.image.width/2
		actual_distance = util.distance(self.position, other_object.position)

		return (actual_distance <= collision_distance)

	def collides_with_any(self, objs):
		col = False
		for obj in objs:
			if self.collides_with(obj):
				col = True
		return col

	def intersect(self, walls):
		col = False
		for s in self.sensor:
			s.t = 1000
			for w in walls:
				s.collides(self.x, self.y, w)
				if s.t < 15:
					col = True
		return col

	# Process input
	def process(self, dt, n):
		force_x = 0
		force_y = 0
		angle_radians = -math.radians(self.rotation)

		if n == 0: # nothing
			self.velocity_x /= (1 + self.friction)
			self.velocity_y /= (1 + self.friction)
		if n == 1: # turn left
			self.rotation -= self.rotate_speed * dt
			self.rotation = self.rotation % 360
			n = 2
		if n == 3: # turn right
			self.rotation += self.rotate_speed * dt
			self.rotation = self.rotation % 360
			n = 2
		if n == 4: # forward
			force_x = math.cos(angle_radians) * self.speed
			force_y = math.sin(angle_radians) * self.speed
		if n == 4: # back
			force_x = -math.cos(angle_radians) * self.speed
			force_y = -math.sin(angle_radians) * self.speed
		
		self.velocity_x = force_x
		self.velocity_y = force_y

		if abs(self.velocity_x) > self.max_speed:
			self.velocity_x -= force_x
		if abs(self.velocity_y) > self.max_speed:
			self.velocity_y -= force_y
			

	def update(self, dt):
		super(Vehicle, self).update(dt)

		self.friction = 0.1
		# print(self.friction, self.velocity_x, self.velocity_y)
		if self.velocity_x > 0 and self.velocity_y > 0:
			velocity_d = math.atan(self.velocity_y/self.velocity_x)
			self.friction = 0#np.dot(velocity_d, math.radians(self.rotation))
		# Rotate Sensors
		for s in self.sensor:
			s.dx = -math.cos(-math.radians(self.rotation) + s.angle)
			s.dy = -math.sin(-math.radians(self.rotation) + s.angle)
		# Slow Car Down
		# if not self.key_handler[key.UP] and not self.key_handler[key.DOWN] and not self.key_handler[key.LEFT] and not self.key_handler[key.RIGHT]:
		self.process(dt, 0)

		self.key = 0
		if self.key_handler[key.UP]:
			self.process(dt, 4)
			self.key = 4
		if self.key_handler[key.DOWN]:
			self.process(dt, 2)
			self.key = 2
		if self.key_handler[key.LEFT]:
			self.process(dt, 1)
			self.key = 1
		if self.key_handler[key.RIGHT]:
			self.process(dt, 3)
			self.key = 3

	def delete(self):
		super(Vehicle, self).delete()
