from pyglet import resource
from util import center_image


# Tell pyglet where to find the resources
resource.path = ['./resources']
resource.reindex()

# Load the three main resources and get them to draw centered
car_image = resource.image("car.png")
center_image(car_image)

point_image = resource.image("ball.png")
center_image(point_image)

wall_image = resource.image("wall.png")
center_image(wall_image)
